<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'PageController@show');
Route::get('/user-login', 'UserController@user_login')->name('login');
Route::get('/login', 'UserController@login');
Route::get('/logout', 'UserController@logout');
Route::post('/user-authenticate', 'UserController@user_authenticate');
Route::get('/user-logout', 'UserController@user_logout');
Route::post('/authenticate', 'UserController@authenticate');
Route::get('/profile', 'UserController@profile')->middleware('auth');


// For Admin Panel
// ===============================

Route::group(['prefix'=>'/admin', 'middleware'=>'isAdmin', 'namespace'=>'Admin'],function (){
    Route::get('/', 'DashboardController@index');

//    For Users
//    =====================================
    Route::resource('/users', 'UserController');

//    For Site Settings
//    =====================================
    Route::get('/site-settings', 'SiteSettingController@index');
    Route::post('/site-settings', 'SiteSettingController@update');
});

// For Viw Pages
// ===========================

Route::get('/{slug}','PageController@show')->name('page.show');

