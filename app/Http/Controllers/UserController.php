<?php

namespace App\Http\Controllers;

use App\OrderDetails;
use App\ProductOrder;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Validator;
use Illuminate\Validation\Rule;


class UserController extends Controller
{
    public function store(Request $request){
        $validation=Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|unique:users',
            'password' => 'required|confirmed|min:6',
            'phone_no' => 'required',
            'accept_terms_condition'=> 'required'
        ]);

        if ($validation->fails()){
            return redirect()->back()->withErrors($validation);
        }

        $allData=$request->all();
        $allData['password']=bcrypt($request->password);
        $allData['user_type']='user';
        $user=User::create($allData);
        Auth::loginUsingId($user->id,true);
        session()->flash('success_message','Registration successfully. Please verify your email before login');
//        return redirect()->back();
        return redirect()->action('Auth\VerificationController@resend');
    }

    public function login(){
        return view('admin.user.login');
    }

    public function user_login(){
        return view('user.login');
    }

    public function user_register(){
        if (Auth::check()){
            return redirect('/');
        }
        else{
            return view('user.register');
        }
    }

    public function logout(){
        Auth::logout();
        return redirect()->action('UserController@login');
    }

    public function authenticate(Request $request){
        if (Auth::attempt(['email'=>$request->email, 'password'=>$request->password, 'user_type'=>'admin'])){
            return redirect()->action('Admin\DashboardController@index');
        }
        else{
            flash('Invalid username or password')->error();
            return redirect()->back();
        }
    }

    public function user_authenticate(Request $request){

        $validation=Validator::make($request->all(), [
            'credential' => 'required',
        ]);

        if ($validation->fails()){
            return redirect()->back()->withErrors($validation);
        }

        $user=User::where('email',$request->credential)->orWhere('phone_no',$request->credential)->first();
        if (is_null($user)){
            flash('User not found');
            return redirect()->back();
        }
        else{
            Auth::loginUsingId($user->id);
            return redirect()->action('UserController@profile');
        }
    }

    public function user_logout(){
        Auth::logout();
        return redirect('/');
    }

    public function profile(){
        $user=Auth::user();
        return view('user.profile',compact('user'));
    }

    public function edit_profile(){
        $user=Auth::user();
        return view('user.edit_profile',compact('user'));
    }

    public function update_profile(Request $request){
        $user=Auth::user();
        $validation=Validator::make($request->all(), [
            // Ignore user id
            'email' => [
                'required',
                Rule::unique('users')->ignore($user->id),
            ],
        ]);

        if ($validation->fails()){
            return redirect()->back()->withErrors($validation);
        }

        $user->name=$request->name;
        $user->email=$request->email;
        $user->country_code=$request->country_code;
        $user->phone_no=$request->phone_no;
        $user->profession=$request->profession;
        $user->address=$request->address;
        if (isset($request->password)){
            $user->password=bcrypt($request->password);
        }

        if ($request->hasFile('profile_image')){
            Storage::delete($user->profile_image);
            $path=$request->file('profile_image')->store('images');
            $image = Image::make(Storage::get($path))->fit(130, 130)->encode();
            Storage::put($path, $image);
            $user->profile_image=$path;
        }

        if ($request->hasFile('banner_image')){
            Storage::delete($user->banner_image);
            $path=$request->file('banner_image')->store('images');
            $image = Image::make(Storage::get($path))->fit(1110, 250)->encode();
            Storage::put($path, $image);
            $user->banner_image=$path;
        }
        $user->save();
        flash('Profile updated successfully');
        return redirect()->back();
    }

}
