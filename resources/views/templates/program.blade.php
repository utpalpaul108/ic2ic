@extends('layout.app')

@section('page_title',' | ' .$page->page_title)

@section('contents')
    <section id="ic-program">
        <div class="container">
            <p class="ic-p2-font-size">This is a tentative program. Program may be subject to change.</p>
            <div class="row">
                <div class="col-md-6 col-lg-6">
                    <img src="/images/programSchedule.jpg" alt="" class="img-fluid ic-ist-row-img">
                </div>
                <div class="col-md-6 col-lg-6">
                    <img src="/images/top-right.jpg" alt="" class="img-fluid ic-ist-row-img">
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-lg-6">
                    <img src="/images/left-bottom.jpg" alt="" class="img-fluid">
                </div>
                <div class="col-md-6 col-lg-6">
                    <img src="/images/right-bottom.jpg" alt="" class="img-fluid">
                </div>
            </div>
        </div>
    </section>
@endsection
