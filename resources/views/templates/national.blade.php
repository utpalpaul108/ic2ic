@extends('layout.app')

@section('page_title',' | ' .$page->page_title)

@section('contents')
    <section id="ic-faculty-national">
        <div class="container">
            <p class="ic-faculty-list">Faculty List (Alphabetically)</p>
            <table class="table">
                <caption>National Faculties</caption>
                <thead class="thead-dark">
                <tr>
                    <th scope="col">Sl No</th>
                    <th scope="col">Member’s Name</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>01</td>
                    <td>Prof Dr Abdul Kader Akanda</td>
                </tr>
                <tr>
                    <td>02</td>
                    <td>Prof Dr Abdul Wadud Chowdhury
                    </td>
                </tr>
                <tr>
                    <td>03</td>
                    <td>Prof Dr Abdullah Al Shafi Majumder
                    </td>
                </tr>
                <tr>
                    <td>04</td>
                    <td>Dr Abdullah Al-Jamil</td>
                </tr>
                <tr>
                    <td>05</td>
                    <td>Prof Dr Abduz Zaher</td>
                </tr>
                <tr>
                    <td>05</td>
                    <td>Prof Dr Abduz Zaher</td>
                </tr>
                <tr>
                    <td>06</td>
                    <td>Prof Dr Afzalur Rahman</td>
                </tr>
                <tr>
                    <td>07</td>
                    <td>Prof Dr AK Miah</td>
                </tr>
                <tr>
                    <td>08</td>
                    <td>Prof Dr Amal Kumar Choudhury</td>
                </tr>
                <tr>
                    <td>09</td>
                    <td>Dr APM Sohrabuzzaman</td>
                </tr>
                <tr>
                    <td>10</td>
                    <td>Prof Dr AQM Reza</td>
                </tr>
                <tr>
                    <td>11</td>
                    <td>Dr Ashok Kumar Dutta</td>
                </tr>
                <tr>
                    <td>12</td>
                    <td>Prof Dr Baren Chakraborty</td>
                </tr>
                <tr>
                    <td>13</td>
                    <td>Prof Dr Chowdhury Meshkat Ahmed</td>
                </tr>
                <tr>
                    <td>14</td>
                    <td>Dr CM Shaheen Kabir</td>
                </tr>
                <tr>
                    <td>15</td>
                    <td>Dr Dhiman Banik</td>
                </tr>
                <tr>
                    <td>16</td>
                    <td>Prof Dr Dipankar Chandra Nag</td>
                </tr>
                <tr>
                    <td>17</td>
                    <td>Prof Dr Fazila-Tun-Nesa-Malik</td>
                </tr>
                <tr>
                    <td>18</td>
                    <td>Prof Dr Fazlur Rahman</td>
                </tr>
                <tr>
                    <td>19</td>
                    <td>Prof Dr Golam Mohiuddin Faruque</td>
                </tr>
                <tr>
                    <td>20</td>
                    <td>Prof Dr H I Lutfor Rahman</td>
                </tr>
                <tr>
                    <td>21</td>
                    <td>Prof Dr K M H S Sirajul Haque</td>
                </tr>
                <tr>
                    <td>22</td>
                    <td>Dr Kaisar Nasrullah Khan</td>
                </tr>
                <tr>
                    <td>23</td>
                    <td>Dr Khandaker Asaduzzaman</td>
                </tr>
                <tr>
                    <td>24</td>
                    <td>Prof Dr Khandaker Qamrul Islam</td>
                </tr>
                <tr>
                    <td>25</td>
                    <td>Prof Dr M A Bashar</td>
                </tr>
                <tr>
                    <td>26</td>
                    <td>Prof M Atahar Ali</td>
                </tr>
                <tr>
                    <td>27</td>
                    <td>Prof Dr M Maksumul Haq</td>
                </tr>
                <tr>
                    <td>28</td>
                    <td>Dr M Mizanur Rahman</td>
                </tr>
                <tr>
                    <td>29</td>
                    <td>Dr Mahbub Mansur</td>
                </tr>
                <tr>
                    <td>30</td>
                    <td>Dr Mahbubor Rahman</td>
                </tr>
                <tr>
                    <td>31</td>
                    <td>Prof Dr Mashhud Zia Chowdhury</td>
                </tr>
                <tr>
                    <td>32</td>
                    <td>Prof Dr Md Badrul Alam</td>
                </tr>
                <tr>
                    <td>33</td>
                    <td>Dr Md Hasanur Rahman</td>
                </tr>
                <tr>
                    <td>34</td>
                    <td>Prof Dr Md Harisul Hoque</td>
                </tr>
                <tr>
                    <td>35</td>
                    <td>Dr Md Khaled Mohsin</td>
                </tr>
                <tr>
                    <td>36</td>
                    <td>Prof Dr Md Mamunur Rashid</td>
                </tr>
                <tr>
                    <td>37</td>
                    <td>Dr Md Rais Uddin</td>
                </tr>
                <tr>
                    <td>38</td>
                    <td>Prof Dr Md Shahab Uddin Talukder</td>
                </tr>
                <tr>
                    <td>39</td>
                    <td>Dr Md Towhiduzzaman</td>
                </tr>
                <tr>
                    <td>40</td>
                    <td>Prof Dr Md Jalaluddin</td>
                </tr>
                <tr>
                    <td>41</td>
                    <td>Prof Dr Md Nur Hossain</td>
                </tr>
                <tr>
                    <td>42</td>
                    <td>Prof Dr Md Shahabuddin Khan</td>
                </tr>
                <tr>
                    <td>43</td>
                    <td>Dr Md Shahriar Kabir</td>
                </tr>
                <tr>
                    <td>44</td>
                    <td>Prof Dr Md Shakil Gafur</td>
                </tr>
                <tr>
                    <td>45</td>
                    <td>Prof Dr MG Azam</td>
                </tr>
                <tr>
                    <td>46</td>
                    <td>Dr Mir Nesaruddin Ahmed</td>
                </tr>
                <tr>
                    <td>47</td>
                    <td>Prof Dr Mir Jamal Uddin</td>
                </tr>
                <tr>
                    <td>48</td>
                    <td>Prof Dr Mohammad Badiuzzaman</td>
                </tr>
                <tr>
                    <td>49</td>
                    <td>Prof Dr M Liaquat Ali</td>
                </tr>
                <tr>
                    <td>50</td>
                    <td>Dr Mohammad Mizanur Rahman (Asgar Ali)</td>
                </tr>
                <tr>
                    <td>51</td>
                    <td>Dr Mohsin Ahmed</td>
                </tr>
                <tr>
                    <td>52</td>
                    <td>Dr Momenuzzaman</td>
                </tr>
                <tr>
                    <td>53</td>
                    <td>Prof Dr Nazir Ahmed</td>
                </tr>
                <tr>
                    <td>54</td>
                    <td>Prof Dr Nurunnahar Fatema </td>
                </tr>
                <tr>
                    <td>55</td>
                    <td>Prof Dr Ranjit C Khan</td>
                </tr>
                <tr>
                    <td>56</td>
                    <td>Prof Dr Sabina Hashem</td>
                </tr>
                <tr>
                    <td>57</td>
                    <td>Dr Sahela Nasrin</td>
                </tr>
                <tr>
                    <td>58</td>
                    <td>Prof Dr Sajal Bannerjee</td>
                </tr>
                <tr>
                    <td>59</td>
                    <td>Brig Gen Prof Dr Sayed Asif Iqbal</td>
                </tr>
                <tr>
                    <td>60</td>
                    <td>Dr Shams Munwar</td>
                </tr>
                <tr>
                    <td>61</td>
                    <td>Prof Dr SM Mustafa Zaman</td>
                </tr>
                <tr>
                    <td>62</td>
                    <td>Prof Dr Emeritus Sufia Rahman</td>
                </tr>
                <tr>
                    <td>63</td>
                    <td>Prof Dr Syed Ali Ahsan</td>
                </tr>
                <tr>
                    <td>64</td>
                    <td>Dr Tamzeed Ahmed</td>
                </tr>
                <tr>
                    <td>65</td>
                    <td>Prof Dr Touhidul Haque</td>
                </tr>
                <tr>
                    <td>66</td>
                    <td>Prof Dr Triptish Chandra Ghose</td>
                </tr>
                </tbody>
            </table>
        </div>
    </section>
@endsection
