@extends('layout.app')

@section('contents')
    <section class="ic-mid-content py-0">
        <div class="container">
            <div class="ic-banner-content">
                <ul>
                    <li>
                        <img src="/images/ic2ic-logo-small.png" alt="">
                    </li>
                </ul>
                <div class="text-content">
                    <h3 style="font-style:italic">Greetings from IC2IC Dhaka 2019</h3>
                    <p>
                        Ibrahim Cardiac Hospital & Research Institute is pleased to announce the inauguration of Ibrahim Cardiac Conference on Interventional Cardiology (IC2IC), for the first time, which is to be held on Friday, 21 June 2019 at Hotel Pan Pacific Sonargaon, Dhaka, Bangladesh</p>

                    <p style="font-style:italic; font-weight:bold" class="ic-italic-p">This day-long program specially targeted at cardiology fellows and early-career interventional cardiologists, promises to be a unique platform for interactive learning.</p>

                    <p>We have an impressive array of sessions lined up, comprising of live case transmissions and state-of-the-art lectures pertaining to interventional cardiology with particular focus on complex coronary intervention. We will also have interactivecase-based discussions and debates on current hot topics in complex PCI such as CTO, Bifurcation, Left main and CHIP.</p>

                    <p>With an outstanding line-up of overseas and Bangladeshi faculty comprising of leading interventional cardiologists in the region, this program has been designed toadvance your skill set and procedural knowledge in a manner that can be applied today-to-day practice in the cath lab.</p>

                    <p>Please save the date, and we look forward to welcoming you for a career-shaping educational experience at IC2IC Dhaka 2019!</p>

                    <p class="pt-1"><b> Prof Dr Md Saidur Rahman Khan</b>
                        Scientific Secretary, IC2IC 2019</p>

                    <p><b> Prof Dr M Maksumul Haq</b>
                        Organizing Secretary, IC2IC 2019</p>

                    <p><b>Prof Dr MA Rashid</b>
                        Chairman, IC2IC 2019</p>
                </div>
            </div>
        </div>
    </section>
@endsection
