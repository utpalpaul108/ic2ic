<!-- Registration Popup -->
<div class="modal registration-modal fade" id="regModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body text-center my-auto">
                <img src="/images/ic2ic-reg-logo.png" alt="">
                <img src="/images/ic2ic-reg-full-form.png" alt="">
                <h5>Check your registered Information</h5>
                <form action="{{ action('UserController@user_authenticate') }}" method="post">
                    @csrf
                    <div class="form-group">
                        <input type="text" name="credential" class="form-control" id="regEmailIdOrPone" aria-describedby="emailHelp" placeholder="Enter Email Id Or Phone Number" required>
                    </div>
                    <button type="submit" class="ic-reg-btn-modal">Check Now</button>
                </form>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-times" aria-hidden="true"></i>
                </button>
            </div>
        </div>
    </div>
</div>
