<header>
    <!-- banner start -->
    <div class="bd-example ic-banner">
        <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="/images/banner.jpg" class="d-block w-100 banner-img" alt="...">
                    <div class="carousel-caption d-md-block">
                        <img src="/images/ic2ic-logo.png" alt="ic2ic-logo" class="ic-logo">
                        <img src="/images/ic2ic-full-form.png" alt="ic2ic-full-form" class="img-fluid" class="ic-logo-fullform">
                        <p>21 June 2019 <br/>
                            Hotel Pan pacific Sonargaon, Dhaka</p>
                        <img src="/images/ibrahim-cardiac-logo.png" alt="ibrahim-cardiac-logo" class="img-fluid ic-cardiac-logo">
                    </div>
                </div>
                <div class="carousel-item">
                    <img src="/images/banner.jpg" class="d-block w-100 banner-img" alt="...">
                    <div class="carousel-caption d-md-block">
                        <img src="/images/ic2ic-logo.png" alt="ic2ic-logo" class="ic-logo">
                        <img src="/images/ic2ic-full-form.png" alt="ic2ic-full-form" class="img-fluid" class="ic-logo-fullform">
                        <p>21 June 2019 <br/>
                            Hotel Pan pacific Sonargaon, Dhaka</p>
                        <img src="/images/ibrahim-cardiac-logo.png" alt="ibrahim-cardiac-logo" class="img-fluid ic-cardiac-logo">
                    </div>
                </div>
                <div class="carousel-item">
                    <img src="/images/banner.jpg" class="d-block w-100 banner-img" alt="...">
                    <div class="carousel-caption d-md-block">
                        <img src="/images/ic2ic-logo.png" alt="ic2ic-logo" class="ic-logo">
                        <img src="/images/ic2ic-full-form.png" alt="ic2ic-full-form" class="img-fluid" class="ic-logo-fullform">
                        <p>21 June 2019 <br/>
                            Hotel Pan pacific Sonargaon, Dhaka</p>
                        <img src="/images/ibrahim-cardiac-logo.png" alt="ibrahim-cardiac-logo" class="img-fluid ic-cardiac-logo">
                    </div>
                </div>
            </div>

            <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev"><span class="fa fa-angle-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
                <span class="fa fa-angle-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>

    <!-- navbar -->
    <nav class="navbar navbar-expand-lg ic-navbar">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <i class="fa fa-bars" aria-hidden="true"></i>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mx-auto">
                <li class="nav-item {{ request()->is('/') ? 'active' : '' }}">
                    <a class="nav-link" href="/">About IC2IC</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/coming-soon"> About Ibrahim Cardiac</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/coming-soon"> Message from CEO, ICHRI</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/coming-soon"> IC2IC Core committee</a>
                </li>
                <li class="nav-item {{ request()->is('program') ? 'active' : '' }}">
                    <a class="nav-link" href="/program">Program</a>
                </li>
                <li class="nav-item dropdown {{ (request()->is('national') || request()->is('international')) ? 'active' : '' }}">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Faculty<i class="fa fa-angle-down" aria-hidden="true"></i>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="/national">National</a>
                        <a class="dropdown-item" href="/international">International</a>
                    </div>
                </li>
                <li class="nav-item ">
                    <a class="nav-link" href="/coming-soon">Case submission</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/coming-soon">Contact us</a>
                </li>
                <li class="nav-item reg">
                    <a class="nav-link" data-toggle="modal" data-target="#regModal">Registration</a>
                </li>
            </ul>
        </div>
    </nav>
    <!--Mid section Start -->
</header>
