
<div class="sa-aside-left">

    <a href="javascript:void(0)"  onclick="SAtoggleClass(this, 'body', 'sa-shortcuts-expanded')" class="sa-sidebar-shortcut-toggle">
        <img src="/ic_admin/img/avatars/sunny.png" alt="" class="online">
        <span>{{ Auth::user()->name }} <span class="fa fa-angle-down"></span></span>
    </a>
    <div class="sa-left-menu-outer">
        <ul class="metismenu sa-left-menu" id="menu1">
            <li class="{{ request()->is('admin') ? 'active' : '' }}"><!-- first-level -->
                <a class="has-arrow"   href="{{ action('Admin\DashboardController@index') }}" title="Dashboard"><span class="fa fa-lg fa-fw fa-home"></span> <span class="menu-item-parent">Dashboard</span></a>
            </li>

            {{--            For Users    --}}

            <li class="{{ request()->is('admin/users*') ? 'active' : '' }}"><!-- first-level -->
                <a class="has-arrow"   href="" title="User"><span class="fa fa-lg fa-fw fa-sliders"></span> <span class="menu-item-parent"> User</span>
                    <b class="collapse-sign">
                        <em class="fa fa-plus-square-o"></em>
                        <em class="fa fa-minus-square-o"></em>
                    </b>
                </a>
                <ul aria-expanded="true" class="sa-sub-nav collapse">

                    <li class="{{ request()->is('admin/users') ? 'active' : '' }}">
                        <a   href="{{ action('Admin\UserController@index') }}" title="Users"> Users </a>
                    </li>
                    <li class="{{ request()->is('admin/users/create*') ? 'active' : '' }}">
                        <a   href="{{ action('Admin\UserController@create') }}" title="Create user"> Create new user</a>
                    </li>
                </ul>

            </li>

            {{--            For Site Settings          --}}

            <li class="{{ request()->is('admin/site-settings*') ? 'active' : '' }}">
                <a class="has-arrow"   href="" title="FAQ"><span class="fa fa-lg fa-fw fa-list"></span> <span class="menu-item-parent"> Site Settings</span>
                    <b class="collapse-sign">
                        <em class="fa fa-plus-square-o"></em>
                        <em class="fa fa-minus-square-o"></em>
                    </b>
                </a>
                <ul aria-expanded="true" class="sa-sub-nav collapse">
                    <li class="{{ request()->is('admin/site-settings') ? 'active' : '' }}">
                        <a   href="{{ action('Admin\SiteSettingController@index') }}" title="site settings"> Edit Settings </a>
                    </li>
                </ul>
            </li>

        </ul>
    </div>
    <a href="javascript:void(0)" class="minifyme" onclick="SAtoggleClass(this, 'body', 'minified')">
        <i class="fa fa-arrow-circle-left hit"></i>
    </a>
</div>
