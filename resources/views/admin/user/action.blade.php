<a class="btn btn-success btn-xs" href="{{ action('Admin\UserController@edit',$id) }}">Edit</a>
<form action="{{ action('Admin\UserController@destroy',$id) }}" method="post" style="display: inline">
    @csrf
    @method('DELETE')
    <button class="btn btn-danger btn-xs" name="remove_slider_group" type="button" onclick="deleteSliderGroup(this)">Delete</button>
</form>
