@extends('admin.layout.app')

@section('page_title','Admin | Create user')

@section('style')
    <link rel="stylesheet" href="/ic_admin/jasny-bootstrap/css/jasny-bootstrap.min.css">
    <link rel="stylesheet" href="/ic_admin/css/forms.css">
@endsection

@section('contents')
    <ol class="align-items-center sa-page-ribbon breadcrumb" aria-label="breadcrumb" role="navigation">
        <li><span id="refresh" class="btn sa-ribbon-btn sa-theme-btn" data-action="resetWidgets"><i class="fa fa-refresh"></i></span></li>
        <li class="breadcrumb-item"><a href="{{ action('Admin\DashboardController@index')}})">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ action('Admin\UserController@index') }}">All users</a></li>
    </ol>

    <!-- END .sa-page-breadcrumb -->

    <div class="sa-content">
        <div class="d-flex w-100 home-header">
            <div>
                <h1 class="page-header"><i class="fa-fw fa fa-home"></i> Dashboard <span>> Create user</span></h1>
            </div>
        </div>


        <div class="w-100">
            <!-- widget grid -->
            <section id="widget-grid" class="">

                <form id="sliderGroup" action="{{ action('Admin\UserController@store') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <!-- row -->
                    <div class="row">

                        <!-- NEW WIDGET ROW START -->
                        <div class="col-md-9">
                            @include('flash::message')
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget" id="wid-id-5" data-widget-colorbutton="false"	data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-sortable="false">
                                <!-- widget options:
                                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                                data-widget-colorbutton="false"
                                data-widget-editbutton="false"
                                data-widget-togglebutton="false"
                                data-widget-deletebutton="false"
                                data-widget-fullscreenbutton="false"
                                data-widget-custombutton="false"
                                data-widget-collapsed="true"
                                data-widget-sortable="false"

                                -->
                                <header>
                                    <div class="widget-header">
                                        <h2>Crete User </h2>
                                    </div>
                                </header>

                                <!-- widget div-->

                                <div>

                                    <!-- widget content -->
                                    <div class="widget-body">
                                        <fieldset>
                                            <legend>
                                                User Data
                                            </legend>
                                            <div class="form-group">
                                                <label>Name</label>
                                                <input type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="Full Name" required />
                                            </div>
                                            <div class="form-group">
                                                <label>Email</label>
                                                <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email Address" required />
                                            </div>
                                            <div class="form-group">
                                                <label>Phone no</label>
                                                <input type="text" class="form-control" name="phone_no" value="{{ old('phone_no') }}" placeholder="Phone No" required />
                                            </div>
                                            <div class="form-group">
                                                <label>Registration ID</label>
                                                <input type="text" class="form-control" name="registration_id" value="{{ old('registration_id') }}" placeholder="Registration ID" required />
                                            </div>
                                            <div class="form-group">
                                                <label>Password</label>
                                                <input type="password" class="form-control" name="password" placeholder="XXXXXXXXXX" required />
                                            </div>
                                            <div class="form-group">
                                                <label>User Type</label>
                                                <select class="form-control" name="user_type">
                                                    <option value="user">User</option>
                                                    <option value="admin">Admin</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Speciality</label>
                                                <input type="text" class="form-control" name="speciality" value="{{ old('speciality') }}" placeholder="Speciality" required />
                                            </div>
                                            <div class="form-group">
                                                <label>Designation</label>
                                                <input type="text" class="form-control" name="designation" value="{{ old('designation') }}" placeholder="Designation" required />
                                            </div>
                                            <div class="form-group">
                                                <label>Institution</label>
                                                <input type="text" class="form-control" name="institution" value="{{ old('institution') }}" placeholder="Institution" required />
                                            </div>
                                            <div class="form-group">
                                                <label>Address</label>
                                                <textarea rows="5" class="form-control" name="address" required >{{ old('address') }}</textarea>
                                            </div>
                                            <div class="form-group">
                                                <label>Status</label>
                                                <select class="form-control" name="status">
                                                    <option value="active">Active</option>
                                                    <option value="inactive">Inactive</option>
                                                </select>
                                            </div>
                                        </fieldset>

                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <button class="btn btn-default" type="submit">
                                                        <i class="fa fa-send"></i>
                                                        Create
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end widget content -->

                                </div>
                                <!-- end widget div -->

                            </div>
                            <!-- end widget -->

                        </div>
                        <!-- WIDGET ROW END -->

                        <!-- NEW WIDGET ROW START -->
                        <div class="col-md-3">
                            <!-- /well -->
                            <div class="well padding-10">
                                <h5 class="mt-0"><i class="fa fa-tags"></i> Profile Image(130 X 130):</h5>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="box-body text-center">
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-new thumbnail" style="max-width: 200px; max-height: 200px;">
                                                    <img src="http://placehold.it/200x200" width="100%" alt="header image">
                                                </div>
                                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 200px;"></div>
                                                <div>
                                                    <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span>
                                                        <input type="file" name="profile_image">
                                                    </span>
                                                    <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /well -->
                        </div>
                        <!-- WIDGET ROW END -->

                    </div>
                </form>
                <!-- end row -->
            </section>
            <!-- end widget grid -->
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function() {

            // For Multiple Select
            if($.fn.select2) {
                $("select.select2").each(function(){var e=$(this),t=e.attr("data-select-width")||"100%";e.select2({allowClear:!0,width:t}),e=null});
            }
            if ($.fn.select2) {
                $("select.select2").each(function () {
                    var e = $(this), t = e.attr("data-select-width") || "100%";
                    e.select2({allowClear: !0, width: t}), e = null
                });
            }

            // form validation

            $('#sliderGroup').bootstrapValidator({
                feedbackIcons : {
                    valid : 'fa fa-check',
                    invalid : 'fa fa-times',
                    validating : 'fa fa-refresh'
                },
                fields : {
                    name : {
                        validators : {
                            notEmpty : {
                                message : 'User name is required'
                            },
                        }
                    },
                    email : {
                        validators : {
                            notEmpty : {
                                message : 'User email is required'
                            }
                        }
                    },
                    speciality : {
                        validators : {
                            notEmpty : {
                                message : 'User speciality is required'
                            }
                        }
                    },
                    registration_id : {
                        validators : {
                            notEmpty : {
                                message : 'Registration Id is required'
                            }
                        }
                    },
                    designation : {
                        validators : {
                            notEmpty : {
                                message : 'User designation is required'
                            }
                        }
                    },
                    institution : {
                        validators : {
                            notEmpty : {
                                message : 'User institution is required'
                            }
                        }
                    },
                    phone_no : {
                        validators : {
                            notEmpty : {
                                message : 'Phone no is required'
                            }
                        }
                    }
                }
            });

            // end profile form

            $('div.alert').delay(3000).fadeOut(350);

        })
    </script>
    <script src="/ic_admin/jasny-bootstrap/js/jasny-bootstrap.min.js"></script>
@endsection
